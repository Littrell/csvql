import axios from "axios";
import Loader from 'react-loader-spinner';
import { ToastProvider } from 'react-toast-notifications';
import { useState } from "react";

import FileUpload from "./FileUpload";
import QueryResults from "./QueryResults";
import QuerySheet from "./QuerySheet";

import "./App.css";

function App() {
  const serverDomain = "localhost:9000";

  const [ loading, setLoading ] = useState(false);
  const [ tableValues, setTableValues ] = useState();
  const [ currentQuery, setCurrentQuery ] = useState("SELECT * FROM csvql");

  const executeQuery = (query, toast) => {
    setLoading(true);
    query = (query && query.length) ? query : "SELECT * FROM csvql";
    setCurrentQuery(query)
    axios.post(`http://${serverDomain}/query`, { query })
      .then(({ data }) => {
        setTableValues(data);
        setLoading(false);
      })
      .catch(error => {
        if (error.response) {
          toast(error.response.data, { appearance: 'error' });
          console.error({
            message: error.response.data,
            status: error.response.status,
            headers: error.response.headers
          });
        } else if (error.request) {
          toast(error.request, { appearance: 'error' });
        } else {
          toast(error.response.data, { appearance: 'error' });
        }
        setLoading(false);
    });
  };

  const upload = (file, toast) => {
    setLoading(true);
    let formData = new FormData();
    formData.append("file", file);
    axios.post(`http://${serverDomain}/upload`, formData, {
      headers: {
        "Content-Type": "multipart/form-data"
      }
    })
    .then(({ data }) => {
      // NOTE this is a temporary fix
      if (data.length > 10000) {
        setTableValues(data.slice(0, 10000));
      } else {
        setTableValues(data);
      }
      setLoading(false);
    })
    .catch(error => {
      if (error.response) {
        toast(error.response.data, { appearance: 'error' });
        console.error({
          message: error.response.data,
          status: error.response.status,
          headers: error.response.headers
        });
      } else if (error.request) {
        toast(error.request, { appearance: 'error' });
      } else {
        toast(error.response.data, { appearance: 'error' });
      }
      setLoading(false);
    });
  };

  const exportTableValues = (evt) => {
    evt.preventDefault();
    setLoading(true);
    axios.post(`http://${serverDomain}/export`, { query: currentQuery })
      .then(({ data }) => {
        if (data.length) {
          let content = `data:text/csv;charset=utf-8,${data}`;
          let encodedUri = encodeURI(content);
          let link = document.createElement("a");
          link.setAttribute("href", encodedUri);
          link.setAttribute("download", "download.csv");
          document.body.appendChild(link);
          link.click();
          link.remove();
        }
        setLoading(false);
      })
      .catch(error => {
        console.error(error);
        setLoading(false);
        // TODO move file upload to a component and add toast
      });
  };

  return (
    <ToastProvider autoDismiss={true} placement={"bottom-right"}>
      <div className="sheet-container">
        <QuerySheet executeQuery={executeQuery} tableValues={tableValues} />
      </div>
      <div className="results-container">
        <div className="loader-container">
          <Loader type="Puff" color="#00BFFF" height={100} width={100} visible={loading} />
        </div>
        <QueryResults results={tableValues} />
        <div className="toolbar">
          <input
            type="button"
            value="Export"
            onClick={exportTableValues}
          />
          <FileUpload upload={upload} />
        </div>
      </div>
    </ToastProvider>
  );
}

export default App;
