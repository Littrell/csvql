import React, { Fragment, useState } from "react";
import { useToasts } from 'react-toast-notifications';
import AceEditor from 'react-ace';

import "./QuerySheet.css";
import "ace-builds/src-noconflict/mode-sql";
import "ace-builds/src-noconflict/theme-solarized_dark";
import "ace-builds/src-noconflict/ext-language_tools";

function QuerySheet({ executeQuery, tableValues }) {

  const [ query, setQuery ] = useState();

  const { addToast } = useToasts();

  const handleSubmit = evt => {
    evt.preventDefault();
    executeQuery(query, addToast);
  };

  const handleChange = value => {
    setQuery(value);
  };

  return (
    <Fragment>
      <AceEditor
        mode="sql"
        theme="solarized_dark"
        onChange={handleChange}
        onLoad={editor => {
          editor.renderer.setPadding(10);
          editor.container.style.lineHeight = "40px";
          editor.container.style.borderBottomLeftRadius = "5px";
          editor.container.style.borderTopLeftRadius = "5px";
          editor.container.style.display = "inline-block";
          editor.container.style.width = "90%";
          editor.resize();
        }}
        name="query-editor"
        editorProps={{ $blockScrolling: true }}
        setOptions={{
          enableBasicAutocompletion: true,
          enableLiveAutocompletion: true,
          enableSnippets: true,
          fontSize: "16px",
          maxLines: 1,
          minLines: 1,
          placeholder: "SELECT * FROM csvql;",
          showGutter: true
        }}
      />
      <div className="run-submit" onClick={handleSubmit}>Run</div>
    </Fragment>
  );
}

export default QuerySheet;
