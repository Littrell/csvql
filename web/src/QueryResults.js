import React from "react";

import "./QueryResults.css";

function QueryResults({ results }) {

  let columns = [];
  if (results && results.length) {
    columns = results[0]
      .filter(cell => cell.columnName !== "id")
      .map(cell => cell.columnName);
  }

  return results && results.length ? (
    <table>
      <thead>
        <tr>
          {columns.map(column => {
            return (
              <th key={column}>{column}</th>
            );
          })}
        </tr>
      </thead>
      <tbody>
        {results && results.map((row, index) => {
          return (
            <tr key={index}>
              {row.map((cell, index) => {
                return (
                  <td key={cell.columnIndex}>{cell.value}</td>
                );
              })}
            </tr>
          );
        })}
      </tbody>
    </table>
  ) : (
    <div className="no-results">No results. Adjust your query or upload a CSV.</div>
  )
}

export default QueryResults;
