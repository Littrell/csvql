import React from "react";
import { useToasts } from 'react-toast-notifications';

import "./FileUpload.css";

function FileUpload({ upload }) {

  const { addToast } = useToasts();

  return (
    <form
      className="upload-form"
      encType="multipart/form-data"
      onChange={e => upload(e.target.files[0], addToast)}>
      <input type="file" name="file" />
    </form>
  );
}

export default FileUpload;
