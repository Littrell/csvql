# csvql

![screenshot](ss.png)

## Setup and run

### Docker

`docker compose up -d && docker compose logs -f`

The web application serves off of [csvql.localhost](csvql.localhost).  
The server serves off of [localhost:9000](localhost:9000).

### Native

#### Build and start the web application

```bash
cd web
npm install
npm start
```

The web application is served off of [localhost:3000](localhost:3000).

#### Build and start the server

```bash
cd server
./build.sh
./server
```

The server serves off of [localhost:9000](localhost:9000)

## TODO

* Short-term
  * Get the image size down
  * Support multiple users
  * Empty cell formatting
  * Test bad data
  * Pin versions
* Long-term
  * Convert integer and boolean columns to the correct types
  * Clean up the front-end; it could use some abstractions
  * Paginate results > 1000 rows; temporary fix in place
  * Store "Help" information in SQLite as a table?
  * Support all file types that SQLite can import
  * "Add more data"
* Front-end
  * There's probably a better way to download the raw file
* Server
  * Create a metadata table to contain current file information
  * Export: in the case of SELECT *, ignore id column
  * Clean up logic in queryToJson to match export
  * Duplicate columns headers fail
