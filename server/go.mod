module gitlab.com/Littrell/csvql/server

go 1.16

require (
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/mattn/go-sqlite3 v1.14.6
)
