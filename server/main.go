package main

import (
  "net/http"
  "flag"
  "log"
  "fmt"

  "github.com/gorilla/mux"
  "github.com/gorilla/handlers"
  _ "github.com/mattn/go-sqlite3"
)

func main() {

  var portFlag = flag.Int64("port", 9000, "Port to serve off of")
  flag.Parse()

  cors := handlers.CORS(
    handlers.AllowedHeaders([]string{"Content-Type", "X-Requested-With"}),
    handlers.AllowedOrigins([]string{"*"}),
    handlers.AllowedMethods([]string{"POST", "OPTIONS"}),
  )

  router := mux.NewRouter().StrictSlash(true)

  router.Use(cors)

  router.HandleFunc("/query", QueryPost).Methods("POST")
  router.HandleFunc("/query", QueryOPTIONS).Methods("OPTIONS")
  router.HandleFunc("/export", ExportPOST).Methods("POST")
  router.HandleFunc("/export", ExportOPTIONS).Methods("OPTIONS")
  router.HandleFunc("/upload", UploadPOST).Methods("POST")

  log.Printf("Listening on localhost:%d", *portFlag)
  log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", *portFlag), router))
}
