package main

import (
  "encoding/json"
  "database/sql"
  "io/ioutil"
  "net/http"
  "os/exec"
  "strings"
  "log"
  "fmt"
  "os"

  _ "github.com/mattn/go-sqlite3"
)

func UploadPOST(w http.ResponseWriter, r *http.Request) {
  log.Println("POST /upload")

  w.Header().Set("Content-Type", "application/json")

  // Limit the request to 200 MB
  r.Body = http.MaxBytesReader(w, r.Body, 200 * 1024 * 1024)

  // Get the file and handler for the upload
  file, handler, err := r.FormFile("file")
  if err != nil {
    log.Println("Error creating file handler")
    http.Error(w, err.Error(), http.StatusBadRequest)
    return
  }
  defer file.Close()

  // Check that it has a .csv extension
  filenameSplit := strings.Split(handler.Filename, ".")
  extension := filenameSplit[len(filenameSplit)-1]
  if extension != "csv" {
    http.Error(w, "File must be a CSV", http.StatusBadRequest)
    return
  }

  // Create a temporary file, likely in /tmp
  tmp, err := ioutil.TempFile("", handler.Filename)
  if err != nil {
    log.Println("Error creating temporary file")
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }
  defer os.Remove(tmp.Name())

  // Read the file to a byte array
  fileBytes, err := ioutil.ReadAll(file)
  if err != nil {
    log.Println("Error reading the file to a byte array")
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }

  // Write btye array to temporary file
  tmp.Write(fileBytes)

  // Close the temporary file
  err = tmp.Close()
  if err != nil {
    log.Println("Error closing temporary file")
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }

  dbPath := "/tmp/csvql.db"

  os.Remove(dbPath)

  // TODO this fails if the CSV has duplicate column headers
  // Construct command to import
  // the flat file into SQLite
  cmd := exec.Command(
    "sqlite3", dbPath,
    "-cmd", ".mode csv",
    "-cmd", fmt.Sprintf(".import %s csvql", tmp.Name()),
  )

  // Run the import
  err = cmd.Run()
  if err != nil {
    log.Println("Error running the import")
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }

  // Connect to the newly created database
  db, err := sql.Open("sqlite3", "file:"+dbPath+"?mode=ro")
  if err != nil {
    log.Println("Error connecting to the newly created database")
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }
  defer db.Close()

  // Query data and send it back
  data, err := queryToJson(db, "SELECT * FROM csvql")
  if err != nil {
    log.Println("Error selecting the inital dataset")
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }

  json.NewEncoder(w).Encode(data)
}
