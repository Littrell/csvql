package main

import (
  "encoding/json"
  "encoding/csv"
  "database/sql"
  "net/http"
  "log"
  "os"

  _ "github.com/mattn/go-sqlite3"
)

func ExportPOST(w http.ResponseWriter, r *http.Request) {
  log.Println("POST /export")

  w.Header().Set("Content-Disposition", "attachment;filename=download.csv")
  w.Header().Set("Content-Type", "application/csv")

  var queryReq QueryRequest

  err := json.NewDecoder(r.Body).Decode(&queryReq)
  if err != nil {
    http.Error(w, err.Error(), http.StatusBadRequest)
    return
  }

  db, err := sql.Open("sqlite3", "file:/tmp/csvql.db?mode=ro")
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }

  rows, err := db.Query(queryReq.Query)
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }

  data := [][]string{}
  columns, err := rows.ColumnTypes()
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }
  columnRow := []string{}
  for _, column := range columns {
    columnRow = append(columnRow, column.Name())
  }
  data = append(data, columnRow)

  for rows.Next() {
    rawRowData := make([][]byte, len(columns))
    rawCellData := make([]interface{}, len(columns))
    for i, _ := range columns {
      rawCellData[i] = &rawRowData[i]
    }

    err = rows.Scan(rawCellData...)
    if err != nil {
      http.Error(w, err.Error(), http.StatusInternalServerError)
      return
    }

    row := make([]string, len(columns))
    for i, raw := range rawRowData {
      if raw == nil {
        row[i] = "\\N"
      } else {
        row[i] = string(raw)
      }
    }

    data = append(data, row)
  }

  if len(data) > 0 {
    os.Remove("/tmp/download.csv")
    file, err := os.Create("/tmp/download.csv")
    if err != nil {
      http.Error(w, err.Error(), http.StatusInternalServerError)
      return
    }
    defer file.Close()

    csvWriter := csv.NewWriter(file)
    defer csvWriter.Flush()

    for _, row := range data {
      err = csvWriter.Write(row)
      if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
      }
    }
    http.ServeFile(w, r, "/tmp/download.csv")
    return
  } else {
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }
}

func ExportOPTIONS(w http.ResponseWriter, r *http.Request) {
  w.WriteHeader(http.StatusOK)
}
