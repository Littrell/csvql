package main

import (
  "encoding/json"
  "database/sql"
  "net/http"
  "log"

  _ "github.com/mattn/go-sqlite3"
)

func QueryPost(w http.ResponseWriter, r *http.Request) {
  log.Println("POST /query")

  w.Header().Set("Content-Type", "application/json")

  var queryReq QueryRequest

  err := json.NewDecoder(r.Body).Decode(&queryReq)
  if err != nil {
    http.Error(w, err.Error(), http.StatusBadRequest)
    return
  }

  db, err := sql.Open("sqlite3", "file:/tmp/csvql.db?mode=ro")
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }

  data, err := queryToJson(db, queryReq.Query)
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }

  json.NewEncoder(w).Encode(data)
}

func QueryOPTIONS(w http.ResponseWriter, r *http.Request) {
  w.WriteHeader(http.StatusOK)
}
