package main

type CellInfo struct {
  ColumnIndex int         `json:"columnIndex"`
  ColumnName  string      `json:"columnName"`
  Value       interface{} `json:"value"`
}

type ColumnInfo struct {
  Cid       int
  Name      string
  Type      string
  NotNull   int
  DfltValue string
  Pk        int
}

type QueryRequest struct {
  Query string `json:"query"`
}
