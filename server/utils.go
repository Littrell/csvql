package main

import "database/sql"

func queryToJson(db *sql.DB, query string, args ...interface{}) ([][]CellInfo, error) {
  var rows [][]CellInfo

  results, err := db.Query(query, args...)
  if err != nil {
    return nil, err
  }

  for results.Next() {
    columns, err := results.ColumnTypes()
    if err != nil {
      return nil, err
    }

    values := make([]interface{}, len(columns))
    cells := []CellInfo{}
    for i, column := range columns {
      var v interface{}

      switch column.DatabaseTypeName() {
        case "text":
          v = new(string)
        default:
          v = new(interface{})
      }

      cell := CellInfo{
        ColumnIndex: i,
        ColumnName: column.Name(),
        Value: v,
      }
      cells = append(cells, cell)
      values[i] = v
    }

    err = results.Scan(values...)
    if err != nil {
      return nil, err
    }

    rows = append(rows, cells)
  }

  return rows, nil
}
